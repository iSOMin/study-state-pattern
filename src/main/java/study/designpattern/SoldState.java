package study.designpattern;

public class SoldState implements State {
	 
    GumballMachine gumballMachine;
 
    public SoldState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
       
	public void insertQuarter() {
		System.out.println("동전을 넣을 수 없습니다. 알맹이가 나가고 있습니다.");
	}
 
	public void ejectQuarter() {
		System.out.println("이미 알맹이를 뽑으셨습니다.");
	}
 
	public void turnCrank() {
		System.out.println("손잡이는 한 번만 돌려주세요.");
	}
 
	public void dispense() {
		gumballMachine.releaseBall();
		if (gumballMachine.getCount() > 0) {
			gumballMachine.setState(gumballMachine.getNoQuarterState());
		} else {
			System.out.println("알맹이가 매진되었습니다.");
			gumballMachine.setState(gumballMachine.getSoldOutState());
		}
	}
 
	public String toString() {
		return "알맹이 판매중";
	}
}
