package study.designpattern;

public class NoQuarterState implements State {
    GumballMachine gumballMachine;
 
    public NoQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
 
	public void insertQuarter() {
		System.out.println("동전을 넣으셨습니다.");
		gumballMachine.setState(gumballMachine.getHasQuarterState());
	}
 
	public void ejectQuarter() {
		System.out.println("동전을 반환할 수 없습니다. 아직 동전을 넣지 않으셨습니다.");
	}
 
	public void turnCrank() {
		System.out.println("손잡이를 돌릴 수 없습니다. 아직 동전을 넣지 않으셨습니다.");
	 }
 
	public void dispense() {
		System.out.println("동전을 먼저 넣어주세요.");
	} 
 
	public String toString() {
		return "동전 없음 / 동전 투입을 기다림";
	}
}
