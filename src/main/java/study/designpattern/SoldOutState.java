package study.designpattern;

public class SoldOutState implements State {
    GumballMachine gumballMachine;
 
    public SoldOutState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
 
	public void insertQuarter() {
		System.out.println("동전을 넣을 수 없습니다. 현재 매진 상태입니다.");
	}
 
	public void ejectQuarter() {
		System.out.println("동전을 반환할 수 없습니다. 아직 동전을 넣지 않으셨습니다.");
	}
 
	public void turnCrank() {
		System.out.println("손잡이를 돌릴 수 없습니다. 아직 동전을 넣지 않으셨습니다.");
	}
 
	public void dispense() {
		System.out.println("알맹이가 나갈 수 없습니다. 현재 매진 상태입니다.");
	}
 
	public String toString() {
		return "알맹이 매진";
	}
}